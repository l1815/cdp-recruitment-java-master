# Leroy Merlin Exercise

This document identifies all the step of exercise execution

## Refactoring package

At the beginning, all classes were in the same package. I created the following packages:

 * **controller** : use-side part, all exposed API should be on this package
 * **service** : business logic part, all business rules are applied here
 * **provider** : service-side part, all database access are on this package
 * **model** : reference all POJOs
 
## FIX: using the delete button works but elements comes back when i refresh the page 

The *@Transactional* annotation was set on *(readOnly = true)* so it wasn't possible to update/create/delete anything on prodiver.

## FIX: Adding review does not work

Add stars too. In addition, using the PUT REST verb isn't what we want here: IHM only purpose to add comment or update number of stars. In this context, API should only expose an API in PATCH REST verb to update only comment/stars.
Because if I want to change other element, with PUT method I'm able to do this (security risk).

About the fix:
 * I create a *findById* method on [EventRepository](./../src/main/java/adeo/leroymerlin/cdp/provider/EventRepository.java) to get an event by id
 * I create a *save* method on [EventRepository](./../src/main/java/adeo/leroymerlin/cdp/provider/EventRepository.java) to store event
 * Two methods on [EventService](./../src/main/java/adeo/leroymerlin/cdp/service/EventService.java) are created to get event on database and update it (if event isn't found, nothing happen)
 * On controller, on API is exposed for each method

No now we can only PATCH comment/stars and not update an entire event.

## New Feature

For this part I just add a stream filter to get matching events (I could use directly JPA syntax, but I was asked to code it in Java). To be more user friendly the search is case insensitive.
I also add the filter on IHM part, so users can filter current events.

## Bonus part

In this part I just change getters of POJO (which are not used by JPA) so the getter method return size of an attribute list.

## Unit tests

I add test for all steps: controller/service/provider and also integration test (total coverage 97.9%).


