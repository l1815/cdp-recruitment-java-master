'use strict';

angular.module('myevent', [
    'angular-input-stars'
])
.factory('EventService', EventService)
.controller('EventsController', EventsController);

function EventService($http){
    return {
        deleteEvent:deleteEvent,
        getEvents:getEvents,
        updateStars: updateStars,
        updateComment: updateComment,
        eventSearch: eventSearch
    };

    function deleteEvent(id){
        return $http.delete('/api/events/' + id);
    }

    function getEvents(){
        return $http.get('/api/events/')
            .then(getEventsComplete);

        function getEventsComplete(response){
            return response.data;
        }
    }

    function updateStars(event){
        return $http.patch('/api/events/updateNbStars/' + event.id + "/" + event.nbStars);
    }
    
    function updateComment(event){
        return $http.patch('/api/events/updateComment/' + event.id + "/" + event.comment);
    }
    
    function eventSearch(text){
        return $http.get('/api/events/search/' + text)
        	.then(getEventsComplete);

        function getEventsComplete(response){
        	return response.data;
        }
    }
}

function EventsController(EventService){
    var vm = this;
    vm.deleteEvent = deleteEvent;
    vm.updateStars = updateStars;
    vm.updateComment = updateComment;
    vm.eventSearch = eventSearch;

    activate();

    function activate() {
        return EventService.getEvents()
        .then(function(events) {
            vm.events = events;
            return vm.events;
        });
    }

    function deleteEvent(event){
        var index = vm.events.indexOf(event);
        return EventService.deleteEvent(event.id)
            .then(function() {
                vm.events.splice(index, 1);
            });
    }

    function updateStars(event){
        return EventService.updateStars(event);
    }
    
    function updateComment(event){
        return EventService.updateComment(event);
    }
    
    function eventSearch(text){
    	
    	if(text === "") {
    		activate();
    	} else {
            return EventService.eventSearch(text)
            .then(function(events) {
                vm.events = events;
                return vm.events;
            });   		
    	}
    }
}