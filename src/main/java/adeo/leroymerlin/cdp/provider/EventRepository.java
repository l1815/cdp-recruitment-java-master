package adeo.leroymerlin.cdp.provider;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import adeo.leroymerlin.cdp.model.Event;

/**
 * The Interface EventRepository: implement by framework on starting
 */
@Transactional
public interface EventRepository extends Repository<Event, Long> {

	/**
	 * Delete by id.
	 *
	 * @param eventId the event id
	 */
	void deleteById(Long eventId);

	/**
	 * Find all event.
	 *
	 * @return the list of events
	 */
	List<Event> findAllBy();

	/**
	 * Save.
	 *
	 * @param event (created if this param doesn't have id, otherwise, updated)
	 * @return the event created/updated
	 */
	Event save(Event event);

	/**
	 * Find by id.
	 *
	 * @param id of event to find
	 * @return the optional event (empty of not found)
	 */
	Optional<Event> findById(long id);

}
