package adeo.leroymerlin.cdp.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.provider.EventRepository;

/**
 * Class EventService: the business class of app. All funcionals rules are applied here
 */
@Service
public class EventService {

	/** The event repository: persistence repository to store/retreive data */
	private final EventRepository eventRepository;

	/**
	 * Instantiates a new event service.
	 *
	 * @param eventRepository the event repository
	 */
	@Autowired
	public EventService(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}

	/**
	 * Gets all the events from database.
	 *
	 * @return all the events
	 */
	public List<Event> getEvents() {
		return this.eventRepository.findAllBy();
	}

	/**
	 * Delete a specific event
	 *
	 * @param id of event to delete
	 */
	public void delete(Long id) {
		this.eventRepository.deleteById(id);
	}

	/**
	 * Gets the filtered events from query
	 *
	 * @param query to apply for filteriing
	 * @return the filtered events
	 */
	public List<Event> getFilteredEvents(String query) {
		final List<Event> events = this.eventRepository.findAllBy();

		// Filter the events list in pure JAVA here
		return events.stream().filter(event ->
		event.getBands().stream().filter(band ->
		band.getMembers().stream().anyMatch(member -> member.getName().toLowerCase().contains(query.toLowerCase()))).findAny().isPresent()
				).collect(Collectors.toList());
	}

	/**
	 * Update event's comment.
	 *
	 * @param id of event to update
	 * @param comment to apply
	 */
	public void	 updateEventComment(long id, String comment) {
		final Optional<Event> optEvent = this.eventRepository.findById(id);
		if(optEvent.isPresent()) {
			final Event event = optEvent.get();
			event.setComment(comment);
			this.eventRepository.save(event);
		}
	}

	/**
	 * Update event's stars.
	 *
	 * @param id of event to update
	 * @param nbStars to apply
	 */
	public void	 updateEventStars(long id, Integer nbStars) {
		final Optional<Event> optEvent = this.eventRepository.findById(id);
		if(optEvent.isPresent()) {
			final Event event = optEvent.get();
			event.setNbStars(nbStars);
			this.eventRepository.save(event);
		}
	}
}
