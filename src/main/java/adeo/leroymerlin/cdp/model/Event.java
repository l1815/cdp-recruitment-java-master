package adeo.leroymerlin.cdp.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Event {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;

	private String title;

	private String imgUrl;

	@OneToMany(fetch=FetchType.EAGER)
	private Set<Band> bands;

	private Integer nbStars;

	private String comment;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return String.format("%s [%d]", this.title, this.bands.size());
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImgUrl() {
		return this.imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public Set<Band> getBands() {
		return this.bands;
	}

	public void setBands(Set<Band> bands) {
		this.bands = bands;
	}

	public Integer getNbStars() {
		return this.nbStars;
	}

	public void setNbStars(Integer nbStars) {
		this.nbStars = nbStars;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
