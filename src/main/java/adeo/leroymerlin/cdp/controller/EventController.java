package adeo.leroymerlin.cdp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.service.EventService;

/**
 * Class EventController: allow to patch/delete/retreive events by an exposed API
 */
@RestController
@RequestMapping("/api/events")
public class EventController {

	/** The event service: business service */
	private final EventService eventService;

	/**
	 * Instantiates a new event controller.
	 *
	 * @param eventService the event service
	 */
	@Autowired
	public EventController(EventService eventService) {
		this.eventService = eventService;
	}

	/**
	 * Find all stored events.
	 *
	 * @return list of event
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Event> findEvents() {
		return this.eventService.getEvents();
	}

	/**
	 * Find a specific event from query.
	 *
	 * @param query the query to find specific event
	 * @return list of event
	 */
	@RequestMapping(value = "/search/{query}", method = RequestMethod.GET)
	public List<Event> findEvents(@PathVariable String query) {
		return this.eventService.getFilteredEvents(query);
	}

	/**
	 * Delete event.
	 *
	 * @param id of event to delete
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteEvent(@PathVariable Long id) {
		this.eventService.delete(id);
	}

	/**
	 * Update event comment.
	 *
	 * @param id of event to update
	 * @param comment to store on event
	 */
	@RequestMapping(value = "updateComment/{id}/{comment}", method = RequestMethod.PATCH)
	public void updateEventComment(@PathVariable Long id, @PathVariable String comment) {
		this.eventService.updateEventComment(id, comment);
	}

	/**
	 * Update event stars.
	 *
	 * @param id of event to update
	 * @param nbStars to store on event
	 */
	@RequestMapping(value = "updateNbStars/{id}/{nbStars}", method = RequestMethod.PATCH)
	public void updateEventStars(@PathVariable Long id, @PathVariable Integer nbStars) {
		this.eventService.updateEventStars(id, nbStars);
	}
}
