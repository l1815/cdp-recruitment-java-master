package adeo.leroymerlin.cdp.integration;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.util.ReflectionTestUtils;

import adeo.leroymerlin.cdp.helper.EventHelper;
import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.provider.EventRepository;

/**
 * This class test the entire application (all relative controllers, services and providers).
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EventIntegrationTest {

	/** The REST template to call application API. */
	@Autowired
	private TestRestTemplate template;

	/** The repository to access database. */
	@Autowired
	EventRepository eventRepository;

	/** The port of the application. */
	@Value("${local.server.port}")
	private int port;

	/** App URL to initialize on "before each". */
	private String appUrl;

	/**
	 * Initialize application URL and allow rest templare to use PATCH verb.
	 */
	@BeforeEach
	void setUp() {
		this.appUrl = "http://localhost:" + this.port + "/api/events/";
		this.template.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
	}

	/**
	 * Ensure that all event in DB are retreived on GET (all) API call.
	 */
	@Test
	void whenfindEvents_thenRetreiveEvents() {

		// When
		final ResponseEntity<Event[]> response = this.template.getForEntity(this.appUrl, Event[].class);

		// Then
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().length).isEqualTo(5);
	}


	/**
	 * Ensure that specific event in DB is retreived on GET (with query) API call.
	 */
	@Test
	void whenfindEventsWithQuery_thenRetreiveSpecificEvent() {

		// When
		final ResponseEntity<Event[]> response = this.template.getForEntity(String.format("%s/search/%s", this.appUrl, "jarvis"), Event[].class);

		// Then
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().length).isEqualTo(1);
		Assertions.assertThat(ReflectionTestUtils.getField(response.getBody()[0], "title"))
		.isEqualTo("GrasPop Metal Meeting [5]");
	}


	/**
	 * Ensure that created event is deleted on DELETE API call.
	 */
	@Test
	void withEventsInDB_whenDeleteEvent_thenEventNotExist() {

		// With
		final Event event1 = EventHelper.generateEvent();
		this.eventRepository.save(event1);

		// When
		this.template.delete(String.format("%s%s", this.appUrl, event1.getId()));

		// Then
		Assertions.assertThat(this.eventRepository.findAllBy().size()).isEqualTo(5);
		Assertions.assertThat(this.eventRepository.findById(event1.getId()).isPresent()).isFalse();
	}

	/**
	 * Ensure that comment of event is updated on PATCH (comment) API call.
	 */
	@Test
	void whenUpdateEventComment_thenCommentChangeInDB() {

		// With
		final Event event = this.eventRepository.findAllBy().get(0);
		event.setComment("old comment");
		this.eventRepository.save(event);

		// When
		this.template.patchForObject(String.format("%s/updateComment/%s/%s", this.appUrl, event.getId(), "new comment"), null, Void.class);

		// Then
		final Event resultEvent = this.eventRepository.findById(event.getId()).get();
		Assertions.assertThat(resultEvent)
		.usingRecursiveComparison()
		.ignoringFields("comment")
		.isEqualTo(event);

		Assertions.assertThat(resultEvent.getComment()).isEqualTo("new comment");
	}

	/**
	 * Ensure that number of starts of event is updated on PATCH (nbStars) API call.
	 */
	@Test
	void whenUpdateEventNbStars_thenNbStarsChangeInDB() {

		// With
		final Event event = this.eventRepository.findAllBy().get(0);
		event.setNbStars(1);
		this.eventRepository.save(event);

		// When
		this.template.patchForObject(String.format("%s/updateNbStars/%s/%d", this.appUrl, event.getId(), 2), null, Void.class);

		// Then
		final Event resultEvent = this.eventRepository.findById(event.getId()).get();
		Assertions.assertThat(resultEvent)
		.usingRecursiveComparison()
		.ignoringFields("nbStars")
		.isEqualTo(event);

		Assertions.assertThat(resultEvent.getNbStars()).isEqualTo(2);
	}
}
