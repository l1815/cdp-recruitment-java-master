package adeo.leroymerlin.cdp.helper;

import java.util.HashSet;
import java.util.Set;

import adeo.leroymerlin.cdp.model.Band;
import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.model.Member;

/**
 * Helper class to create event
 */
public class EventHelper {

	/**
	 * Generate event.
	 *
	 * @return the event
	 */
	public static Event generateEvent() {
		final Event event = new Event();
		event.setComment("a simple comment");
		event.setImgUrl("http://img.png");
		event.setNbStars(3);
		event.setTitle("a simple title");
		return event;
	}

	/**
	 * Generate event with member.
	 *
	 * @param name the name
	 * @return the event
	 */
	public static Event generateEventWithMember(String name) {

		final Event event = generateEvent();

		final Band band = EventHelper.generateBand(name);

		final Member member = EventHelper.generateMember(name);
		final Set<Member> members = new HashSet<>();
		members.add(member);

		band.setMembers(members);
		final Set<Band> bands = new HashSet<>();
		bands.add(band);

		event.setBands(bands);

		return event;
	}

	/**
	 * Generate band.
	 *
	 * @param name the name
	 * @return the band
	 */
	public static Band generateBand(String name) {
		final Band band = new Band();
		band.setName(name);
		return band;
	}

	/**
	 * Generate member.
	 *
	 * @param name the name
	 * @return the member
	 */
	public static Member generateMember(String name) {
		final Member member = new Member();
		member.setName(name);
		return member;
	}

}
