package adeo.leroymerlin.cdp.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import adeo.leroymerlin.cdp.helper.EventHelper;
import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.provider.EventRepository;

/**
 * This class test {@link EventServiceTest} only, service is mocked.
 */
public class EventServiceTest {

	/** The event service : service to test. */
	private EventService eventService;

	/** The event repository: mocked repository (not tested anywhere because we don't need to test JPA framework) */
	private EventRepository eventRepository;

	/**
	 * Initialize mocked repository and service
	 */
	@BeforeEach
	void setUp() {
		this.eventRepository = Mockito.mock(EventRepository.class);
		this.eventService = new EventService(this.eventRepository);
	}

	/**
	 * Ensure that all event in mocked repository are retreived on get events method call.
	 */
	@Test
	void whenGetEvents_thenRetreiveEvents() {

		// With
		final List<Event> events = Arrays.asList(EventHelper.generateEvent(), EventHelper.generateEvent());
		Mockito.when(this.eventRepository.findAllBy()).thenReturn(events);

		// When
		final List<Event> result = this.eventService.getEvents();

		// Then
		Assertions.assertThat(result.size()).isEqualTo(2);
		Mockito.verify(this.eventRepository, Mockito.times(1)).findAllBy();
	}

	/**
	 * Ensure that specific event in mocked repository is retreived on get filtered events call.
	 */
	@Test
	void whenGetFilteredEvents_thenRetreiveSpecificEvents() {

		// With
		final Event eventToFind = EventHelper.generateEventWithMember("member To Find");
		final Event otherEventToNotFind = EventHelper.generateEventWithMember("you souldn't find me");
		final List<Event> events = Arrays.asList(eventToFind, otherEventToNotFind);
		Mockito.when(this.eventRepository.findAllBy()).thenReturn(events);

		// When
		final List<Event> result = this.eventService.getFilteredEvents("to find");

		// Then
		Assertions.assertThat(result.size()).isEqualTo(1);
		Assertions.assertThat(result.get(0)).isEqualTo(eventToFind);
		Mockito.verify(this.eventRepository, Mockito.times(1)).findAllBy();
	}

	/**
	 * Ensure that no event in mocked repository is retreived on get filtered events call when nothing match.
	 */
	@Test
	void whenGetFilteredEvents_thenNoEvents() {

		// With
		final Event otherEventToNotFind1 = EventHelper.generateEventWithMember("you souldn't find me");
		final Event otherEventToNotFind2 = EventHelper.generateEventWithMember("you souldn't find me too");
		final List<Event> events = Arrays.asList(otherEventToNotFind1, otherEventToNotFind2);
		Mockito.when(this.eventRepository.findAllBy()).thenReturn(events);

		// When
		final List<Event> result = this.eventService.getFilteredEvents("to find");

		// Then
		Assertions.assertThat(result.size()).isEqualTo(0);
		Mockito.verify(this.eventRepository, Mockito.times(1)).findAllBy();
	}

	/**
	 * Ensure that delete method of repository is called when we delete event from service
	 */
	@Test
	void whenDeleteEvent_thenServiceCalledOnTime() {

		// When
		this.eventService.delete(1L);

		// Then
		Mockito.verify(this.eventRepository, Mockito.times(1)).deleteById(1L);
	}

	/**
	 * Ensure that update comment method of repository is called when we update a comment from service
	 */
	@Test
	void whenUpdateEventComment_thenCommentChange() {

		// With
		final Event event = EventHelper.generateEvent();
		Mockito.when(this.eventRepository.findById(1L)).thenReturn(Optional.of(event));
		final ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);

		// When
		this.eventService.updateEventComment(1L, "new comment");

		// Then
		Mockito.verify(this.eventRepository, Mockito.times(1)).findById(1L);
		Mockito.verify(this.eventRepository, Mockito.times(1)).save(captor.capture());
		final Event capturedEvent = captor.getValue();
		Assertions.assertThat(capturedEvent).usingRecursiveComparison()
		.ignoringFields("comment")
		.isEqualTo(event);

		Assertions.assertThat(capturedEvent.getComment()).isEqualTo("new comment");

	}

	/**
	 * Ensure that update comment method of repository isn't called when we update an unexisting event
	 * from service
	 */
	@Test
	void whenUpdateUnexistingEventComment_thenNoSavingAction() {

		EventHelper.generateEvent();
		Mockito.when(this.eventRepository.findById(1L)).thenReturn(Optional.empty());

		// When
		this.eventService.updateEventComment(1L, "new comment");

		// Then
		Mockito.verify(this.eventRepository, Mockito.times(1)).findById(1L);
		Mockito.verify(this.eventRepository, Mockito.times(0)).save(Mockito.any());

	}

	/**
	 * Ensure that update number of stars method of repository is called when we update number of stars
	 *  from service
	 */
	@Test
	void whenUpdateEventNbStars_thenNbStarsChange() {

		// With
		final Event event = EventHelper.generateEvent();
		Mockito.when(this.eventRepository.findById(1L)).thenReturn(Optional.of(event));
		final ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);

		// When
		this.eventService.updateEventStars(1L, 4);

		// Then
		Mockito.verify(this.eventRepository, Mockito.times(1)).findById(1L);
		Mockito.verify(this.eventRepository, Mockito.times(1)).save(captor.capture());
		final Event capturedEvent = captor.getValue();
		Assertions.assertThat(capturedEvent).usingRecursiveComparison()
		.ignoringFields("nbStars")
		.isEqualTo(event);

		Assertions.assertThat(capturedEvent.getNbStars()).isEqualTo(4);

	}

	/**
	 * Ensure that update number of stars method of repository isn't called when we update an
	 * unexisting event from service
	 */
	@Test
	void whenUpdateUnexistingEventNbStars_thenNoSavingAction() {

		EventHelper.generateEvent();
		Mockito.when(this.eventRepository.findById(1L)).thenReturn(Optional.empty());

		// When
		this.eventService.updateEventStars(1L, 4);

		// Then
		Mockito.verify(this.eventRepository, Mockito.times(1)).findById(1L);
		Mockito.verify(this.eventRepository, Mockito.times(0)).save(Mockito.any());

	}
}
