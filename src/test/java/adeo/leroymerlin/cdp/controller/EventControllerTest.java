package adeo.leroymerlin.cdp.controller;

import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import adeo.leroymerlin.cdp.helper.EventHelper;
import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.service.EventService;

/**
 * This class test {@link EventControllerTest} only, service is mocked.
 */
public class EventControllerTest {

	/** The event controller : controller to test. */
	private EventController eventController;

	/** The event service: mocked service (not tested here) */
	private EventService eventService;

	/**
	 * Initialize mocked service and controller
	 */
	@BeforeEach
	void setUp() {
		this.eventService = Mockito.mock(EventService.class);
		this.eventController = new EventController(this.eventService);
	}

	/**
	 * Ensure that all events in mocked service are retreived when find events
	 */
	@Test
	void whenfindEvents_thenRetreiveEvents() {

		// With
		final List<Event> events = Arrays.asList(EventHelper.generateEvent(), EventHelper.generateEvent());
		Mockito.when(this.eventService.getEvents()).thenReturn(events);

		// When
		final List<Event> result = this.eventController.findEvents();

		// Then
		Assertions.assertThat(result.size()).isEqualTo(2);
		Mockito.verify(this.eventService, Mockito.times(1)).getEvents();
	}

	/**
	 * Ensure that specific events in mocked service is retreived when find events with query
	 */
	@Test
	void whenfindEventsWithQuery_thenRetreiveSpecificEvents() {

		// With
		final List<Event> events = Arrays.asList(EventHelper.generateEvent(), EventHelper.generateEvent());
		Mockito.when(this.eventService.getFilteredEvents("query")).thenReturn(events);

		// When
		final List<Event> result = this.eventController.findEvents("query");

		// Then
		Assertions.assertThat(result.size()).isEqualTo(2);
		Mockito.verify(this.eventService, Mockito.times(1)).getFilteredEvents("query");
	}

	/**
	 * Ensure that service delete method of service is called when we delete event from controller
	 */
	@Test
	void whenDeleteEvent_thenServiceCalledOnTime() {

		// When
		this.eventController.deleteEvent(1L);

		// Then
		Mockito.verify(this.eventService, Mockito.times(1)).delete(1L);
	}


	/**
	 * Ensure that service update comment method of service is called when we update a comment from contoller
	 */
	@Test
	void whenUpdateEventComment_thenServiceCalledOnTime() {

		// When
		this.eventController.updateEventComment(1L, "comment");

		// Then
		Mockito.verify(this.eventService, Mockito.times(1)).updateEventComment(1L, "comment");
	}

	/**
	 * Ensure that service update number of stars method of service is called when we update the number
	 * of stars from contoller
	 */
	@Test
	void whenUpdateEventNbStars_thenServiceCalledOnTime() {

		// When
		this.eventController.updateEventStars(1L, 5);

		// Then
		Mockito.verify(this.eventService, Mockito.times(1)).updateEventStars(1L, 5);
	}
}
